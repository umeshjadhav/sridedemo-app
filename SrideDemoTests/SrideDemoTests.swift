//
//  SrideDemoTests.swift
//  SrideDemoTests
//
//  Created by Umesh on 10/01/19.
//  Copyright © 2019 UJ. All rights reserved.
//

import XCTest
@testable import SrideDemo

class SrideDemoTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testIsPrimeNo() {
        let no = 11
        XCTAssertTrue(no.isPrime, "\(no) is Prime Number")
        XCTAssertFalse(no.isPrime, "\(no) is not Prime Number")
    }
//    func testExample() {
//        // This is an example of a functional test case.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//    }
//
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }

}
