//
//  WebServiceManager.swift
//  SrideDemo
//
//  Created by Umesh on 10/01/19.
//  Copyright © 2019 UJ. All rights reserved.
//

import UIKit
import Foundation

class WebserviceManager: NSObject {

    class func getRequest(url: URL, callBack:@escaping (Any) -> Void) {
        let request = URLRequest(url:url)
        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
            guard let data = data, error == nil else { return }
            do {
                let jsonObject = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                callBack(jsonObject)
            } catch let error as NSError {
                print(error)
            }
        }
        task.resume()
    }
}
