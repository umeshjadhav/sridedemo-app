//
//  ViewController.swift
//  SrideDemo
//
//  Created by Umesh on 10/01/19.
//  Copyright © 2019 UJ. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {

    fileprivate var datePicker : UIDatePicker!
    fileprivate var weatherData = Weather(currentTemprature: 0.0, minTemprature: 0.0, maxTemprature: 0.0)

    @IBOutlet weak var chooseDateTextField: UITextField!
    @IBOutlet weak var minTempeeratureLabel: UILabel!
    @IBOutlet weak var currentTemperatureLabel: UILabel!
    @IBOutlet weak var maxTemperatureLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    //MARK:- Methods
     func configureDatePicker(_ textField : UITextField) {
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePicker.Mode.date
        textField.inputView = self.datePicker
        textField.inputAccessoryView = createToolBar()
    }

    private func createToolBar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = .black
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, space, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        return toolBar
    }

    private func getWeatherForDate() {
        WebserviceManager.getRequest(url: URL(string: API.GET_WEATHER)!) { (response) in
            print(response)
            guard let currently = ((response as AnyObject).value(forKey: "currently")) else { return }
            self.weatherData.currentTemprature = (currently as AnyObject).value(forKey: "temperature") as! Double
            guard let daily = ((response as AnyObject).value(forKey: "daily")) else { return }
            if let data = ((daily as AnyObject).value(forKey: "data")) as? NSArray {
                self.weatherData.minTemprature = (data[0] as AnyObject).value(forKey: "temperatureMin") as! Double
                self.weatherData.maxTemprature = (data[0] as AnyObject).value(forKey: "temperatureMax") as! Double
                self.showTemperature()
            }

        }
    }
    private func showAlertForInvalidDate() {
        let alert = UIAlertController(title: "Warning", message: "Date is not prime", preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
        }))
        present(alert, animated: true, completion: nil)
    }
    private func showTemperature() {
       DispatchQueue.main.async {
        self.currentTemperatureLabel.text = String(self.weatherData.currentTemprature.convertToCelsius()) + "º"
        self.minTempeeratureLabel.text = String(self.weatherData.minTemprature.convertToCelsius()) + "º"
        self.maxTemperatureLabel.text = String(self.weatherData.maxTemprature.convertToCelsius()) + "º"
        }
    }
    //MARK:- Actions
    @objc func doneClick() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        chooseDateTextField.text = dateFormatter.string(from: datePicker.date)
        chooseDateTextField.resignFirstResponder()
        dateFormatter.dateFormat = "d"
        let day = dateFormatter.string(from: datePicker.date)
        (Int(day)?.isPrime)! ? getWeatherForDate() : showAlertForInvalidDate()
    }
    @objc func cancelClick() {
        chooseDateTextField.resignFirstResponder()
    }
}

extension WeatherViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.configureDatePicker(textField)
    }
}

//amit@sride.co
