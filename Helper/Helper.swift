//
//  Utility.swift
//  SrideDemo
//
//  Created by Umesh on 10/01/19.
//  Copyright © 2019 UJ. All rights reserved.
//

import UIKit

extension Int {
    var isPrime: Bool {
        guard self >= 2     else { return false }
        guard self != 2     else { return true  }
        guard self % 2 != 0 else { return false }
        return !stride(from: 3, through: Int(sqrt(Double(self))), by: 2).contains { self % $0 == 0 }
    }
}

extension Double {
    func convertToCelsius() -> Int {
    return Int(5.0 / 9.0 * (Double(self) - 32.0))
    }
}
