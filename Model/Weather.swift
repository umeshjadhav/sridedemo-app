//
//  Weather.swift
//  SrideDemo
//
//  Created by Umesh on 10/01/19.
//  Copyright © 2019 UJ. All rights reserved.
//

import Foundation

struct Weather {
    var currentTemprature: Double
    var minTemprature: Double
    var maxTemprature: Double
}
