//
//  Constants.swift
//  SrideDemo
//
//  Created by Umesh on 10/01/19.
//  Copyright © 2019 UJ. All rights reserved.
//

import Foundation

struct API {
    static let GET_WEATHER = "https://api.darksky.net/forecast/96bfe5a7db7727bdfd313e773de353a1/18.5204,73.8567?exclude=hourly,minutely"
}
